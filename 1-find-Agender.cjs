let data = require('./2-arrays-logins.cjs');

const allPeopleWhoAgender = data.filter(({ gender }) => gender === "Agender").map((data) => data.first_name + " " + data.last_name);

console.log(allPeopleWhoAgender);