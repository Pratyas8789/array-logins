let data = require('./2-arrays-logins.cjs');

const addedFullName = data.map((user) => {
    return {
        ...user,
        full_name: user.first_name + " " + user.last_name
    }
});
console.log(addedFullName);