let data = require('./2-arrays-logins.cjs');

const sum = data.reduce((acc, data) => {
    let arrOfEmail = (data.ip_address).split('.');
    if (arrOfEmail.length === 4) {
        let num = +arrOfEmail[1];
        if (!isNaN(num)) {
            acc += num;
        }
    }
    return acc;
}, 0)
console.log(sum);