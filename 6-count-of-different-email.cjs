let data=require('./2-arrays-logins.cjs');

const countOfEmail=data.reduce((acc,data)=>{
    const email=data["email"];
    if(email.includes(".org")){
        acc[".org"]++;
    }
    if(email.includes(".au")){
        acc[".au"]++;
    }
    if(email.includes(".com")){
        acc[".com"]++;
    } 
    return acc;
},{
    ".org":0,
    ".au":0,
    ".com":0
});
console.log(countOfEmail);
